#include <iostream>
#include <string.h>
#include <map>
#include <list>
#include <vector>
#include <stdio.h>
#include <avtVector.h>
#include <avtIntervalTree.h>
#include <algorithm>

#ifndef AVT_BLOCK_CHOWDER_H
#define AVT_BLOCK_CHOWDER_H


class DomainBlock
{
public:
    enum LeafBlockType {NONE, UNIFORM, NON_UNIFORM, INTERNAL, X_MIN, X_MAX, Y_MIN, Y_MAX, Z_MIN, Z_MAX};
    
    static std::vector<std::string> nameMap;
    static std::map<int, DomainBlock *> leafMap;
    static void CreateBlockInfo(std::vector<DomainBlock*> &v, int nDom, avtIntervalTree *it,
                                bool subdivUniform, int nX, int nY, int nZ, double pct,
                                bool skipSharedFaceStats=true);
    static int TotalNumLeaves(std::vector<DomainBlock*> &v);
    static void Dump(DomainBlock *b, ostream &s, int lvl, bool rawNums=false);
    static void Dump(std::vector<DomainBlock*> &v, ostream &s, int lvl, bool rawNums=false);
    static DomainBlock * GetBlockFromGID(std::vector<DomainBlock*> &v, int gid);
    static DomainBlock * GetLeaf_FIX_THIS(std::vector<DomainBlock*> &v, const avtVector &p);
    
    DomainBlock();
    DomainBlock(int d, float *b, std::string n="");
    DomainBlock(int d, double *b, std::string n="");
    virtual ~DomainBlock();

    bool Inside(float *p, int &id) const;

    //bool operator< (const DomainBlock &x) const { return (dom==x.dom ? (sub < x.sub) : dom < x.dom); }

    void setBBox(float *b) { for(int i = 0; i < 6; i++) bbox[i]=b[i];}
    void setBBox(double *b) { for(int i = 0; i < 6; i++) bbox[i]=b[i];}
    void setNm(const std::string &n);

    void SubdivideUniform(int nx, int ny, int nz, LeafBlockType bt=UNIFORM);
    void SubdivideFaces(int nx, int ny, int nz, float pct);
    void dump(ostream& s, int lvl, bool rawNums=false, std::string indent="") const;
    void dump(ostream& s, bool rawNums=false) const;
    void dumpBBox(ostream& s) const;
    
    bool InBBox(float *p) const;
    bool InBBox(const avtVector &p) const;
    bool InBBox(const avtVector &p, const double &tol) const;
    void GetLeaves(std::vector<DomainBlock *> &v);
    int NumLeafs() const;

    DomainBlock * GetLeafFromIndex(int idx);
    DomainBlock * GetLeaf(const avtVector &p);
    void GetBBox(float *bb);
    void GetExtents(float *ext);
    void GetExtents(avtVector &ext);
    
    bool AddBlockData(DomainBlock *dst, int numICs, int numIters, int totalNumICs);
    void UnifyData();
    int GetDataIdxFromPct(float p)
    {
        if (data.size() == 0) return -1;
        for (int i = 0; i < data.size(); i++)
            if (p < data[i].pct)
                return i;
            else
                p -= data[i].pct;
        return -1;
    }

    bool BlocksShareFace(DomainBlock *blk);

    class blockData
    {
    public:
        blockData(DomainBlock *b, int n, int ni, int tni):blk(b),numICs(n),numIters(ni),
                                                          totalNumICs(tni),pct(0.0),avgIt(0.0) {}
        blockData() {numICs=numIters=totalNumICs=0; blk=NULL; pct=avgIt=0.0f;}

        void computeAvg() {pct = (float)numICs/(float)totalNumICs; avgIt = (float)numIters/(float)numICs;}
        
        DomainBlock *blk;
        int numICs, numIters, totalNumICs;
        float pct, avgIt;
        
        void dump(ostream& s, bool rawNums=false) const
        {
            char str[64];
            std::string n = (blk ? blk->nm : "<NO-BLK>");
            
            if (rawNums)
                sprintf(str, "(%s %d %d)", n.c_str(), numICs, numIters);
            else
                sprintf(str, "(%s %.2f %4.2f)", n.c_str(), pct, avgIt);
            s<<str;
        }

        static bool cmp(const blockData &a, const blockData &b) {return a.pct < b.pct;}
        static bool rcmp(const blockData &a, const blockData &b) {return a.pct > b.pct;}
    };
    
    //members.
    DomainBlock *parent;
    int dom, sub, gid;
    int minIt, maxIt;
    double expIt, bbox[6];
    bool skipSharedFaceStats;
    std::string nm;
    LeafBlockType leafBlockType;
    std::vector<DomainBlock *> children;
    std::vector<blockData> data;

private:
    DomainBlock * AddChild(float *bb, const char *n);
    int SetIDs(int id);
    void DoSubdivideFaces(int nx, int ny, int nz, float pct);
    void DoSubdivideUniform(int nx, int ny, int nz);
};

#endif
