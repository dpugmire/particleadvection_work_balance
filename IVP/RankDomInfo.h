#ifndef AVT_RANK_DOM_INFO_H
#define AVT_RANK_DOM_INFO_H

class rankInfo2
{
public:
    class blockInfo {
    public:
        blockInfo() {dom=0;iters=0.0f; loadCost = 0.0f; it_cost=0.0f;}
        blockInfo(int d, float i, float l) : dom(d), iters(i), loadCost(l) {it_cost=0.0f;}
        int dom;
        float iters, loadCost, it_cost;
    };

    rankInfo2() {rank=0; iters=0.0;}
    rankInfo2(int r, float i, std::vector<blockInfo> b) :rank(r),iters(i),blocks(b) {}
    
    int rank;
    float iters;
    float t_cost, it_cost, io_cost;
    std::vector<blockInfo> blocks;

    static bool d_cmp(blockInfo x, blockInfo y) {return x.dom < y.dom;}
    static bool i_cmp(blockInfo x, blockInfo y) {return x.iters < y.iters;}

    static bool t_cost_cmp(rankInfo2 x, rankInfo2 y) {return x.t_cost < y.t_cost;}
    static bool t_cost_rcmp(rankInfo2 x, rankInfo2 y) {return y.t_cost < x.t_cost;}
    static bool it_cost_cmp(rankInfo2 x, rankInfo2 y) {return x.it_cost < y.it_cost;}
    static bool it_cost_rcmp(rankInfo2 x, rankInfo2 y) {return y.it_cost < x.it_cost;}

    static std::vector<int> getThresh(std::vector<rankInfo2> &v, float val, bool GT, bool sortIt)
    {
        std::vector<int> indices;
        if (sortIt)
        {
        }
        else
        {
            for (int i = 0; i < v.size(); i++)
                if ((GT && v[i].t_cost >= val) || (!GT && v[i].t_cost <= val))
                    indices.push_back(i);           
        }

        return indices;
    }

    static void printIt(const std::vector<rankInfo2> &v)
    {
        if (PAR_Rank() != 0) return;
        cout<<"RankInfo2: R t_c (it_c io_c) [(dom it_c io_c)...]"<<endl;
        for (int i = 0; i < v.size(); i++)
        {
            cout<<v[i].rank<<" "<<v[i].t_cost<<" ("<<v[i].it_cost<<" "<<v[i].io_cost<<") ";
            cout<<v[i].iters<<" [";
            for (int j = 0; j < v[i].blocks.size(); j++)
                cout<<"("<<v[i].blocks[j].dom<<" "<<v[i].blocks[j].it_cost<<" "<<v[i].blocks[j].loadCost<<")";
            cout<<"]"<<endl;
        }
        float m=v[0].t_cost, M=v[0].t_cost;
        float tot=0.0;
        for (int i = 0; i < v.size(); i++)
        {
            tot += v[i].t_cost;
            if (v[i].t_cost < m) m = v[i].t_cost;
            if (v[i].t_cost > M) M = v[i].t_cost;
        }
        float mean = tot/(float)v.size();
        float sigma = 0.0;
        for (int i = 0; i < v.size(); i++)
            sigma += ((mean-v[i].t_cost)*(mean-v[i].t_cost));
        sigma = sqrt(sigma/(float)v.size());
        cout<<"mMa:s= ["<<m<<" "<<M<<" "<<mean<<" : "<<sigma<<"]"<<endl;
    }
};

class rankInfo
{
public:
    rankInfo() {rank=0; bal=0.0f;}
    rankInfo(int r, float b, std::vector<int> d) : rank(r), bal(b), doms(d) {}

    int rank;
    float bal;
    std::vector<int> doms;

    static bool cmp(rankInfo x, rankInfo y) {return x.bal < y.bal;}
    static bool rcmp(rankInfo x, rankInfo y) {return y.bal < x.bal;}
    static void printIt(const std::vector<rankInfo> &v)
    {
        if (PAR_Rank() != 0) return;
        cout<<"RankInfo: R Bal Doms"<<endl;
        for (int i = 0; i < v.size(); i++)
            cout<<v[i].rank<<" "<<v[i].bal<<" "<<v[i].doms<<endl;
    }
    
    static bool cmp2(std::pair<int,float> x, std::pair<int,float> y) {return x.second < y.second;}
    static bool rcmp2(std::pair<int,float> x, std::pair<int,float> y) {return y.second < x.second;}
    static std::vector<int> getThresh(std::vector<rankInfo> &v, float val, bool GT, bool sortIt)
    {
        std::vector<int> indices;

        if (sortIt)
        {
            std::vector<std::pair<int,float> >x;
            for (int i = 0; i < v.size(); i++)
                if ((GT && v[i].bal >= val) || (!GT && v[i].bal <= val))
                    x.push_back(std::pair<int,float>(i,v[i].bal));

            if (PAR_Rank()==0) cout<<"getThresh: "<<x.size()<<endl;
            sort(x.begin(), x.end(), (GT ? rcmp2 : cmp2));
            for (int i = 0; i < x.size(); i++)
                indices.push_back(x[i].first);
        }
        else
        {
            for (int i = 0; i < v.size(); i++)
                if ((GT && v[i].bal >= val) || (!GT && v[i].bal <= val))
                    indices.push_back(i);
        }
        
        return indices;
    }
};

class domInfo
{
public:
    domInfo() {dom=0; pop=0.0f;}
    domInfo(int d, float p) : dom(d), pop(p) {}
    
    int dom;
    float pop;

    static bool cmp(domInfo x, domInfo y) {return x.pop < y.pop;}
    static bool rcmp(domInfo x, domInfo y) {return y.pop < x.pop;}
    static void printIt(const std::vector<domInfo> &v)
    {
        if (PAR_Rank() != 0) return;
        cout<<"DomInfo:"<<endl;
        for (int i = 0; i < v.size(); i++)
            cout<<v[i].dom<<" "<<v[i].pop<<endl;
    }
};


#endif
