/*****************************************************************************
 *
 * Copyright (c) 2000 - 2012, Lawrence Livermore National Security, LLC
 * Produced at the Lawrence Livermore National Laboratory
 * LLNL-CODE-442911
 * All rights reserved.
 *
 * This file is  part of VisIt. For  details, see https://visit.llnl.gov/.  The
 * full copyright notice is contained in the file COPYRIGHT located at the root
 * of the VisIt distribution or at http://www.llnl.gov/visit/copyright.html.
 *
 * Redistribution  and  use  in  source  and  binary  forms,  with  or  without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of  source code must  retain the above  copyright notice,
 *    this list of conditions and the disclaimer below.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this  list of  conditions  and  the  disclaimer (as noted below)  in  the
 *    documentation and/or other materials provided with the distribution.
 *  - Neither the name of  the LLNS/LLNL nor the names of  its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT  HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR  IMPLIED WARRANTIES, INCLUDING,  BUT NOT  LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND  FITNESS FOR A PARTICULAR  PURPOSE
 * ARE  DISCLAIMED. IN  NO EVENT  SHALL LAWRENCE  LIVERMORE NATIONAL  SECURITY,
 * LLC, THE  U.S.  DEPARTMENT OF  ENERGY  OR  CONTRIBUTORS BE  LIABLE  FOR  ANY
 * DIRECT,  INDIRECT,   INCIDENTAL,   SPECIAL,   EXEMPLARY,  OR   CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT  LIMITED TO, PROCUREMENT OF  SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF  USE, DATA, OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER
 * CAUSED  AND  ON  ANY  THEORY  OF  LIABILITY,  WHETHER  IN  CONTRACT,  STRICT
 * LIABILITY, OR TORT  (INCLUDING NEGLIGENCE OR OTHERWISE)  ARISING IN ANY  WAY
 * OUT OF THE  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 *****************************************************************************/

// ************************************************************************* //
//                            avtAChowderICAlgorithm.h                         //
// ************************************************************************* //

#ifndef AVT_ACHOWDER_IC_ALGORITHM_H
#define AVT_ACHOWDER_IC_ALGORITHM_H

#ifdef PARALLEL

#include <avtICAlgorithm.h>
#include <avtParICAlgorithm.h>
#include <VisItStreamUtil.h>

/*ROB*/
#include <vtkRectilinearGrid.h>
#include <avtStreamlineIC.h>
#include <RankDomInfo.h>

class DomainBlock;

// ****************************************************************************
// Class: avtAChowderICAlgorithm
//
// Purpose:
//    A class for performing serial streamline integration.
//
// Programmer: Dave Pugmire
// Creation:   Mon Jan 26 13:25:58 EST 2009
//
// Modifications:
//
//   Dave Pugmire, Tue Aug 18 09:10:49 EDT 2009
//   Add ability to restart integration of streamlines.
//
//   Dave Pugmire, Thu Sep 24 13:52:59 EDT 2009
//   Change Execute to RunAlgorithm.
//
//   Hank Childs, Fri Jun  4 19:58:30 CDT 2010
//   Use avtStreamlines, not avtStreamlineWrappers.
//
//   Hank Childs, Sun Jun  6 12:25:31 CDT 2010
//   Change the names of several methods to reflect the new emphasis in
//   particle advection, as opposed to streamlines.  Also change reference
//   from avtStreamlineFilter to avtPICSFilter.
//
//   Hank Childs, Sun Jun  6 14:54:08 CDT 2010
//   Rename class "IC" from "SL", to reflect the emphasis on integral curves,
//   as opposed to streamlines.
//
//   Dave Pugmire, Thu Dec  2 11:21:06 EST 2010
//   Add CheckNextTimeStepNeeded.
//
// ****************************************************************************

class avtAChowderICAlgorithm : public avtParICAlgorithm
{
    public:
        avtAChowderICAlgorithm(avtPICSFilter *picsFilter, int count);
        virtual ~avtAChowderICAlgorithm();

        virtual const char*       AlgoName() const {return "ACHW";}
        virtual void              Initialize(std::vector<avtIntegralCurve *> &);
        //virtual void              RestoreInitialize(std::vector<avtIntegralCurve *> &, int curTimeSlice);
        virtual void              AddIntegralCurves(std::vector<avtIntegralCurve*> &ics);

    void SetInfo(bool b, int nx, int ny, int nz, bool uniform, float pct,
                 int nSeeds, int nSteps, float minProb, int popularityMethod,
                 int icReq, int stealIC)
    {
        doBalance = b;
        subdivNX = nx; subdivNY = ny; subdivNZ = nz;
        subdivUniform = uniform; subdivPct = pct;
        numTestSeeds = nSeeds;
        maxTestSteps = 1000;
        minProbability = minProb;
        if (popularityMethod == 0)
            popMethod = PROB_TREE;
        else if (popularityMethod == 1)
            popMethod = RANDOM_WALK;
        maxICReq = icReq;
        maxStealIC = stealIC;
    }

protected:
    virtual void              RunAlgorithm();
    virtual void              PreRunAlgorithm();
    bool                      HandleCommunication();
    bool                      CheckMessages();
    void                      ProcessMessages(std::vector<MsgCommData> &msgs);
    void                      CommICs(std::list<avtIntegralCurve *> &l, int dstRank=-1);
    void                      FindICsInDoms(std::list<avtIntegralCurve *> &lout,
                                            std::list<avtIntegralCurve *> &lin,
                                            std::set<int> &reqDoms,
                                            int maxSend);
    void                      CommTerm();
    void                      CommReq();
    virtual void              ReportStatistics(ostream &os);

    enum PopularityMethod {RANDOM_WALK, PROB_TREE};
    


        /*ROB*/
        class Neighbor
        {
            public:
                int   ID; 
                int   totalSeeds;
                int   enteredSeeds;
                int   sumIt;
                int   minIt;
                int   maxIt;
                float p;         //from parent block to this, p% of seeds end up here.
                float avgIt;

                Neighbor(int id)
                {
                    ID           = id;
                    p            = 0;
                    minIt        = -1;
                    maxIt        = -1;
                    sumIt        = 0;
                    enteredSeeds = 0;
                }
                void particleEntered(int iterations)
                {
                    enteredSeeds++;
                    if(iterations < minIt || minIt == -1)
                        minIt = iterations;
                    if(iterations > maxIt)
                        maxIt = iterations;

                    sumIt += iterations;
                    avgIt = 1.0*sumIt/enteredSeeds;
                }   
                void setTotal(int total)
                {   
                    totalSeeds = total;
                    p = 1.0*enteredSeeds/totalSeeds;
                }   
        }; 

    void SetNumberOfTestSeeds(int n) {numTestSeeds = n;}
    void SetMaxTestSteps(int n) {maxTestSteps = n;}
    void SetMinProbability(float p) {minProbability = p;}

    std::map<int, Neighbor *> neighbors;
    std::set<int> lazyLoadBlocks;
    std::map<int, std::set<int> > rankToBlockMap;
    std::vector<int> ranksWithMyBlocks;
    //THERE IS SOME MEMORY ISSUE HERE. REMOVING THIS MEMBER CAUSES PROBLEMS.
    std::vector<int> myBlocks;

    int numTestSeeds;
    int maxTestSteps;
    float minProbability;

    std::vector<int> domIntegrateSteps, rankIntegrateSteps;
    
    int DomainToRank2(BlockIDType &blk);
    void UpdateBlockAssignments();
    void ComputeBalance(std::vector<float> &balance);
    void AssignBlock(int r, int b);
    std::vector<std::vector<int> > blockAssignments;

    void BuildDomainInfo(std::vector<domInfo> &di);
    void BuildRankInfo(std::vector<rankInfo> &ri);

    void BuildRankInfo2(std::vector<rankInfo2> &ri);

    void DoRankCentricBalancing();
    void DoRankCentricBalancing2();
    void DoBlockCentricBalancing();

    float LOAD_TIME, ADVECT_TIME;
    int numTestParticlesSteps;

    void DumpBlockStatsData(const std::vector<int> &actualIterations);
    void DumpStats();
    void DumpPythonCode(std::vector<avtIntegralCurve *> &ics);

    PopularityMethod popMethod;
    bool doBalance, subdivUniform;
    int subdivNX, subdivNY, subdivNZ;
    float subdivPct;
    int maxICReq, maxStealIC;
    
    void BalanceWorkload(std::vector<avtIntegralCurve *> &ics);
    void ComputeBlockPopRandomWalk(std::vector<avtIntegralCurve *> &ics);
    void ComputeBlockPopProbTree(std::vector<avtIntegralCurve *> &ics);
    void RunTestPts(int d, int s, std::vector<avtVector> &pts, int **blockData);
    void RunInteriorTestSeeds(int d, int **blockData, std::vector<avtIntegralCurve *> &seedPts);
    avtStreamlineIC * makeIC(const avtVector &p);
    void GenerateTestPts(int d, int s, int nPts, std::vector<avtVector> &pts);
    void GenerateTestPts(int d, std::vector<avtIntegralCurve *> &ics, std::vector<avtVector> &pts);
    std::vector<DomainBlock *> blockInfo;
    std::vector<float> blockPopularity;
    
    std::vector<float> allAdvectTime, allIOTime;
    std::vector<int> allDomIntegrateSteps, allRankIntegrateSteps;

    bool printStuff, printRank0Stuff;
    int NVALS;
    int numBlocksDuplicated;
    int maxCount;
    int totalNumICs, numTerminated;

    int numICRequests, numICReqPosted, numTermMsgs;

    StopWatch tstPtsSW, probSW, upBlkSW, ldBlkSW, addICsSW;
    
    static int TERMINATE, REQUEST;
};

#endif
#endif
