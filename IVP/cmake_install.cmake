# Install script for directory: /sw/sources/visit/focus/builds/branches/chowderPICS/src/avt/IVP

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_ser.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_ser.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_ser.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib" TYPE SHARED_LIBRARY FILES "/sw/sources/visit/focus/builds/branches/chowderPICS/src/lib/libavtivp_ser.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_ser.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_ser.so")
    file(RPATH_REMOVE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_ser.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_ser.so")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_par.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_par.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_par.so"
         RPATH " /sw/focus/openmpi/1.8.3/rhel6.6_gnu4.7.1/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib" TYPE SHARED_LIBRARY FILES "/sw/sources/visit/focus/builds/branches/chowderPICS/src/lib/libavtivp_par.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_par.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_par.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_par.so"
         OLD_RPATH "/sw/sources/visit/focus/thirdparty/trunk/visit/vtk/6.1.0/linux-x86_64_gcc-4.7/lib:/sw/sources/visit/focus/builds/branches/chowderPICS/src/lib:/sw/sources/visit/focus/thirdparty/trunk/visit/mesa/7.10.2/linux-x86_64_gcc-4.7/lib:/sw/focus/openmpi/1.8.3/rhel6.6_gnu4.7.1/lib:"
         NEW_RPATH " /sw/focus/openmpi/1.8.3/rhel6.6_gnu4.7.1/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/2.9.0b/linux-x86_64/lib/libavtivp_par.so")
    endif()
  endif()
endif()

